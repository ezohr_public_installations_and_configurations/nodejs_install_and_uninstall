# Node.js Install And Uninstall ([Uninstall](https://www.positronx.io/how-to-uninstall-node-js-and-npm-from-macos/))

## Prerequisites

> None

## Install (Node.js)

### macOS Safari Application (Node.js)

> Enter: <https://nodejs.org/en/download/current/>
>
> Select: macOS Installer (.pkg) 64bit
>
> Select: Signed SHASums For Release Files

### macOS Terminal Application (Node.js Part 1)

> Validate: [Checksum]

### macOS Finder Application (Node.js)

> Double Click: node-[Version].pkg
>
> Select: Continue
>
> Select: Continue
>
> Select: Agree
>
> Select: Install
>
> Enter: [Password]

## Uninstall (Node.js)

### macOS Terminal Application (Node.js Part 2)

`cd /usr/local/include`

`sudo rm -Rf node`

> Enter: [Password]

`cd /usr/local/lib`

`sudo rm -Rf node_modules`

`cd /usr/local/bin`

`sudo rm -Rf node`

`sudo rm [Node.js Symbolic Links]`
